package com.grapeqin.project.service.impl;

import javax.annotation.Resource;

import com.grapeqin.project.core.AbstractService;
import com.grapeqin.project.dao.DepartmentMapper;
import com.grapeqin.project.model.Department;
import com.grapeqin.project.service.DepartmentService;

import org.springframework.stereotype.Service;


/**
* Created by CodeGenerator on 2020/04/13.
*/
@Service
public class DepartmentServiceImpl extends AbstractService<Department> implements DepartmentService {
@Resource
private DepartmentMapper departmentMapper;

}
